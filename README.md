# Albacross React/Redux task

This repository contains simple application with component `GiphySearch`
that gives the possibilities to search for gifs available in [Giphy](https://giphy.com/).

**Note:** Please beware that API key being used in this application is not
production ready so due to limitations API can respond with following `429`
(Too Many Requests) HTTP error code.

## Prerequisites

- [node.js](https://nodejs.org/en/) (preferable LTS)
- package manager ([yarn](https://yarnpkg.com/en/docs/getting-started) or [npm](https://docs.npmjs.com/getting-started/what-is-npm))

## Install & Start

> Please note that you can use any package manager. This instruction is using `yarn`.

1. Run in terminal `yarn` or `npm install`
2. Run `yarn watch-css` (during development) or `yarn build-css` (to build once)
3. Run `yarn start`
4. \[optional\] Open your browser on http://localhost:3000/ (it should open automatically)

## How to use

Application has only few interactive elements:

- search text input
- previous gif button
- next gif button

Type text to search for gif.

There is also simple caching strategy (only in redux store, until page reload).
Searching same gif more than once should not make additional requests to API.

-------------------------
**Note**: This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
