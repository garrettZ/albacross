import React from 'react';
import { GiphySearchContainer } from './containers/GiphySearchContainer';

const App = () => <GiphySearchContainer />;

export default App;
