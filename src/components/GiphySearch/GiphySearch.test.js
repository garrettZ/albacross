import React from 'react';
import { shallow } from 'enzyme';
import { GiphySearch, RenderGuards } from './GiphySearch';

const props = {
  currentItemIndex: 0,
  searchText: 'searchText',
  error: null,
  isFetching: false,
  gifs: [
    {
      title: 'title',
      url: 'url',
      shareUrl: 'shareUrl',
    },
  ],
  onSearch: jest.fn(),
  onNext: jest.fn(),
  onPrev: jest.fn(),
};

test('should render subcomponents when data is available', () => {
  expect(shallow(<GiphySearch {...props} />)).toMatchSnapshot();
});

describe('RenderGuards', () => {
  test('should render nothing when searchText is empty', () => {
    expect(
      shallow(<RenderGuards {...props} searchText="" />).equals(null)
    ).toEqual(true);
  });

  test('should render error message when error occurred', () => {
    expect(
      shallow(<RenderGuards {...props} error="error" />)
    ).toMatchSnapshot();
  });

  test('should render loader when fetching', () => {
    expect(shallow(<RenderGuards {...props} isFetching />)).toMatchSnapshot();
  });

  test('should render message when there are no gifs', () => {
    expect(shallow(<RenderGuards {...props} gifs={[]} />)).toMatchSnapshot();
  });
});
