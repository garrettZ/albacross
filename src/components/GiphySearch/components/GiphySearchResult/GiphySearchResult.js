import React from 'react';
import { gifShape } from '../../../../types';
import './GiphySearchResult.css';

const GiphySearchResultPropTypes = {
  gif: gifShape,
};

const GiphySearchResult = ({ gif }) => (
  <div className="alb-giphy-search-result">
    <img
      alt={gif.title}
      className="alb-giphy-search-result__image"
      src={gif.url}
    />
    <h2>{gif.title || '[ untitled ]'}</h2>
    <p>{gif.shareUrl}</p>
  </div>
);

GiphySearchResult.propTypes = GiphySearchResultPropTypes;
export { GiphySearchResult };
