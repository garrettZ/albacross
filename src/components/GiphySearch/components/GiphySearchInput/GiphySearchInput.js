import React from 'react';
import { func, number } from 'prop-types';
import debounce from 'lodash/fp/debounce';
import './GiphySearchInput.css';

const GiphySearchInputPropTypes = {
  debounce: number,
  onChange: func.isRequired,
};

class GiphySearchInput extends React.PureComponent {
  searchDebounced = debounce(this.props.debounce, searchText => {
    this.props.onChange(searchText);
  });

  static propTypes = GiphySearchInputPropTypes;

  static defaultProps = {
    debounce: 500,
  };

  componentWillUnmount() {
    this.searchDebounced.cancel();
  }

  handleSearch = ({ target }) => {
    this.searchDebounced(target.value.trim());
  };

  render() {
    return (
      <input
        className="alb-giphy-search-input"
        onChange={this.handleSearch}
        placeholder="Search for GIF"
        type="text"
      />
    );
  }
}

export { GiphySearchInput, GiphySearchInputPropTypes };
