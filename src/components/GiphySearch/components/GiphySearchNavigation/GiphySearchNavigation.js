import React from 'react';
import { number, func } from 'prop-types';
import { Button } from '../../../Button';

import './GiphySearchNavigation.css';

const GiphySearchNavigationPropTypes = {
  currentItemIndex: number.isRequired,
  numberOfItems: number.isRequired,
  onNext: func.isRequired,
  onPrev: func.isRequired,
};

const GiphySearchNavigation = ({
  currentItemIndex,
  numberOfItems,
  onNext,
  onPrev,
}) => (
  <div className="alb-giphy-search-nav">
    <Button disabled={currentItemIndex === 0} onClick={onPrev} text="<" />
    <span className="alb-giphy-search-nav__counter">
      {currentItemIndex + 1} / {numberOfItems}
    </span>
    <Button
      disabled={currentItemIndex === numberOfItems - 1}
      onClick={onNext}
      text=">"
    />
  </div>
);

GiphySearchNavigation.propTypes = GiphySearchNavigationPropTypes;
export { GiphySearchNavigation, GiphySearchNavigationPropTypes };
