import React from 'react';
import { arrayOf, string, node, bool } from 'prop-types';
import { gifShape } from '../../types';
import {
  GiphySearchNavigation,
  GiphySearchNavigationPropTypes,
} from './components/GiphySearchNavigation';
import {
  GiphySearchInput,
  GiphySearchInputPropTypes,
} from './components/GiphySearchInput';
import { GiphySearchResult } from './components/GiphySearchResult';
import './GiphySearch.css';

const GiphySearchPropTypes = {
  gifs: arrayOf(gifShape),
  searchText: string,
  currentItemIndex: GiphySearchNavigationPropTypes.currentItemIndex,
  error: string,
  isFetching: bool.isRequired,
  onSearch: GiphySearchInputPropTypes.onChange,
  onNext: GiphySearchNavigationPropTypes.onNext,
  onPrev: GiphySearchNavigationPropTypes.onPrev,
};

const RenderGuards = ({ gifs, searchText, error, children, isFetching }) => {
  if (!searchText) {
    return null;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (isFetching) {
    return <div>...loading</div>;
  }

  if (!gifs.length) {
    return <div>No gifs for given phrase: {searchText}</div>;
  }

  return children;
};

const GiphySearch = props => {
  const { onSearch, onNext, onPrev, gifs, currentItemIndex } = props;

  return (
    <div className="alb-giphy-search">
      <GiphySearchInput onChange={onSearch} />
      <RenderGuards {...props}>
        <GiphySearchNavigation
          currentItemIndex={currentItemIndex}
          numberOfItems={gifs.length}
          onNext={onNext}
          onPrev={onPrev}
        />
        <GiphySearchResult gif={gifs[currentItemIndex]} />
      </RenderGuards>
    </div>
  );
};

RenderGuards.propTypes = { ...GiphySearchPropTypes, children: node };
GiphySearch.propTypes = GiphySearchPropTypes;
export { GiphySearch, RenderGuards, GiphySearchPropTypes };
