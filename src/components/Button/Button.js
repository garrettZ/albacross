import React from 'react';
import cx from 'classnames';
import { string, bool, func } from 'prop-types';
import './Button.css';

const PropTypes = {
  text: string.isRequired,
  accent: bool,
  disabled: bool,
  onClick: func.isRequired,
};

const DefaultProps = {
  accent: false,
};

const Button = ({ text, accent, ...props }) => (
  <button
    className={cx(['alb-button', accent && 'alb-button--accent'])}
    type="button"
    {...props}
  >
    {text}
  </button>
);

Button.propTypes = PropTypes;
Button.defaultProps = DefaultProps;
export { Button };
