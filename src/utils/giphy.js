export const createGiphyUrl = (searchText, apiKey, limit = 5) =>
  /* eslint-disable-next-line max-len */
  `http://api.giphy.com/v1/gifs/search?q=${searchText}&api_key=${apiKey}&limit=${limit}`;
