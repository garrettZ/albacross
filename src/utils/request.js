const DEFAULT_OPTIONS = {
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
  },
};

export const createRequest = (url, options) =>
  fetch(url, {
    ...DEFAULT_OPTIONS,
    ...options,
  }).then(res => res.json());

export const request = {
  get: (url, options) => createRequest(url, { ...options, type: 'GET' }),
  post: (url, options) => createRequest(url, { ...options, type: 'POST' }),
};
