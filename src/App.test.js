import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

it('should render GiphySearch', () => {
  expect(shallow(<App />)).toMatchSnapshot();
});
