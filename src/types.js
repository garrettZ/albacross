import { shape, string } from 'prop-types';

export const gifShape = shape({
  url: string.isRequired,
  title: string.isRequired,
  shareUrl: string.isRequired,
});
