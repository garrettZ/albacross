import { combineReducers } from 'redux';
import { reducer as giphySearch } from './giphySearch';

const rootReducer = combineReducers({
  giphySearch,
});

export { rootReducer };
