import {
  NEXT,
  PREV,
  SEARCH_CACHED,
  SEARCH_FAILURE,
  SEARCH_REQUEST,
  SEARCH_SUCCESS,
  SET_SEARCH_TEXT,
} from './actionTypes';
import { request } from '../../utils/request';
import { API_KEY } from '../../config';
import { createGiphyUrl } from '../../utils/giphy';
import { getHistory } from './selectors';

export const setSearchText = searchText => ({
  type: SET_SEARCH_TEXT,
  payload: searchText,
});

export const navigateNext = () => ({
  type: NEXT,
});

export const navigatePrev = () => ({
  type: PREV,
});

export const requestGiphy = () => ({
  type: SEARCH_REQUEST,
});

export const requestGiphySuccess = (searchText, gifs) => ({
  type: SEARCH_SUCCESS,
  payload: {
    searchText,
    gifs,
  },
});

export const requestGiphyError = error => ({
  type: SEARCH_FAILURE,
  payload: error,
});

export const requestGiphyCached = cachedGiphy => ({
  type: SEARCH_CACHED,
  payload: cachedGiphy,
});

export const fetchGiphy = searchText => (dispatch, getState) => {
  const cachedGiphy = getHistory(getState())[searchText];

  if (cachedGiphy) {
    dispatch(requestGiphyCached(cachedGiphy));
    return undefined;
  }

  dispatch(requestGiphy());

  return request
    .get(createGiphyUrl(searchText, API_KEY))
    .then(res => {
      // eslint-disable-next-line camelcase
      const gifs = res.data.map(({ images, title, bitly_url }) => ({
        url: images.fixed_height.url,
        title,
        shareUrl: bitly_url,
      }));

      dispatch(requestGiphySuccess(searchText, gifs));
    })
    .catch(error => {
      dispatch(requestGiphyError(error.message || 'Something went wrong...'));
    });
};
