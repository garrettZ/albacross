import {
  SEARCH_REQUEST,
  SEARCH_SUCCESS,
  SEARCH_FAILURE,
  SEARCH_CACHED,
  NEXT,
  PREV,
  SET_SEARCH_TEXT,
} from './actionTypes';

const initialState = {
  search: '',
  currentGifIndex: 0,
  gifs: [],
  history: {},
  error: null,
  uiState: {
    isFetching: false,
  },
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_SEARCH_TEXT:
      return { ...state, search: payload };

    case NEXT:
      return { ...state, currentGifIndex: state.currentGifIndex + 1 };

    case PREV:
      return { ...state, currentGifIndex: state.currentGifIndex - 1 };

    case SEARCH_REQUEST:
      return {
        ...state,
        currentGifIndex: initialState.currentGifIndex,
        gifs: initialState.gifs,
        error: initialState.error,
        uiState: { isFetching: true },
      };

    case SEARCH_SUCCESS:
      return {
        ...state,
        gifs: payload.gifs,
        history: { ...state.history, [payload.searchText]: payload.gifs },
        uiState: { isFetching: false },
      };

    case SEARCH_CACHED:
      return { ...state, gifs: payload, isFetching: false };

    case SEARCH_FAILURE:
      return { ...state, error: payload, uiState: { isFetching: false } };

    default:
      return state;
  }
};

export { reducer };
