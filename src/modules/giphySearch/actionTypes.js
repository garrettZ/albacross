export const SET_SEARCH_TEXT = 'giphySearch/SET_SEARCH_TEXT';
export const SEARCH_REQUEST = 'giphySearch/SEARCH_REQUEST';
export const SEARCH_SUCCESS = 'giphySearch/SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'giphySearch/SEARCH_FAILURE';
export const SEARCH_CACHED = 'giphySearch/SEARCH_CACHED';
export const NEXT = 'giphySearch/NEXT';
export const PREV = 'giphySearch/PREV';
