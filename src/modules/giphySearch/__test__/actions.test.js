import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../actions';

jest.mock('../../../utils/giphy', () => ({
  createGiphyUrl: searchText => searchText,
}));

const mockStore = configureStore([thunk]);

test('navigateNext should match snapshot', () => {
  expect(actions.navigateNext()).toMatchSnapshot();
});

test('navigatePrev should match snapshot', () => {
  expect(actions.navigatePrev()).toMatchSnapshot();
});

test('requestGiphy should match snapshot', () => {
  expect(actions.requestGiphy()).toMatchSnapshot();
});

test('requestGiphySuccess should match snapshot', () => {
  expect(actions.requestGiphySuccess('searchText', ['gifs'])).toMatchSnapshot();
});

test('requestGiphyError should match snapshot', () => {
  expect(actions.requestGiphyError('error')).toMatchSnapshot();
});

test('requestGiphyCached should match snapshot', () => {
  expect(actions.requestGiphyCached(['cached-gifs'])).toMatchSnapshot();
});

describe('fetchGiphy', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });
  const state = {
    giphySearch: {
      search: '',
      currentGifIndex: 0,
      gifs: [],
      history: {},
      error: null,
      uiState: {
        isFetching: false,
      },
    },
  };

  test('should dispatch requestGiphy', () => {
    fetch.mockResponseOnce();
    const store = mockStore(state);
    store.dispatch(actions.fetchGiphy());
    expect(store.getActions()[0]).toEqual(actions.requestGiphy());
  });

  test('should dispatch requestGiphyCached with cached gif', () => {
    const cachedGiphy = ['cached-gif'];
    const store = mockStore({
      giphySearch: { ...state.giphySearch, history: { cachedGiphy } },
    });
    store.dispatch(actions.fetchGiphy('cachedGiphy'));
    expect(store.getActions()[0]).toEqual(
      actions.requestGiphyCached(cachedGiphy)
    );
  });

  test('should make API request with proper searchText', () => {
    fetch.mockResponseOnce();
    const store = mockStore(state);
    const searchText = 'searchText';
    store.dispatch(actions.fetchGiphy(searchText));
    expect(fetch.mock.calls[0][0]).toEqual(searchText);
  });

  test('should dispatch requestGiphySuccess with proper payload', async () => {
    const searchText = 'searchText';
    const gifs = [
      {
        url: 'gif-url',
        title: 'gif-title',
        shareUrl: 'gif-bitly_url',
      },
    ];
    fetch.mockResponseOnce(
      JSON.stringify({
        data: [
          {
            images: { fixed_height: { url: gifs[0].url } },
            title: gifs[0].title,
            bitly_url: gifs[0].shareUrl,
          },
        ],
      })
    );
    const store = mockStore(state);

    await store.dispatch(actions.fetchGiphy(searchText));
    expect(store.getActions()[1]).toEqual(
      actions.requestGiphySuccess(searchText, gifs)
    );
  });

  test('should dispatch requestGiphyError with proper payload', async () => {
    const error = 'fetch-error-mock';
    fetch.mockRejectOnce(new Error(error));
    const store = mockStore(state);

    await store.dispatch(actions.fetchGiphy());
    expect(store.getActions()[1]).toEqual(actions.requestGiphyError(error));
  });
});
