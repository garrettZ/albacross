import { createSelector } from 'reselect';

const rootSelector = state => state.giphySearch;

export const getGifs = createSelector(rootSelector, state => state.gifs);
export const getError = createSelector(rootSelector, state => state.error);
export const getSearchText = createSelector(
  rootSelector,
  state => state.search
);
export const getCurrentItemIndex = createSelector(
  rootSelector,
  state => state.currentGifIndex
);
export const getHistory = createSelector(rootSelector, state => state.history);
const getUIState = state => rootSelector(state).uiState;
export const getIsFetching = createSelector(
  getUIState,
  state => state.isFetching
);
