import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
// eslint-disable-next-line import/no-extraneous-dependencies
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer } from './rootReducer';

const configureStore = preloadedState => {
  const composedEnhancers = composeWithDevTools(
    applyMiddleware(thunkMiddleware)
  );
  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  // eslint-disable-next-line no-process-env
  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./rootReducer', () => store.replaceReducer(rootReducer));
  }

  return store;
};

export { configureStore };
