import React from 'react';
import { connect } from 'react-redux';
import { func } from 'prop-types';
import {
  GiphySearch,
  GiphySearchPropTypes,
} from '../../components/GiphySearch';
import { selectors, actions } from '../../modules/giphySearch';

class GiphySearchContainer extends React.Component {
  static propTypes = {
    gifs: GiphySearchPropTypes.gifs,
    searchText: GiphySearchPropTypes.searchText,
    currentItemIndex: GiphySearchPropTypes.currentItemIndex,
    error: GiphySearchPropTypes.error,
    isFetching: GiphySearchPropTypes.isFetching,
    onNext: GiphySearchPropTypes.onNext,
    onPrev: GiphySearchPropTypes.onPrev,
    setSearchText: func,
    fetchGiphy: func,
  };

  handleSearch = searchText => {
    this.props.setSearchText(searchText);

    if (searchText) {
      this.props.fetchGiphy(searchText);
    }
  };

  render() {
    const {
      currentItemIndex,
      gifs,
      searchText,
      onNext,
      onPrev,
      error,
      isFetching,
    } = this.props;

    return (
      <GiphySearch
        currentItemIndex={currentItemIndex}
        error={error}
        gifs={gifs}
        isFetching={isFetching}
        onNext={onNext}
        onPrev={onPrev}
        onSearch={this.handleSearch}
        searchText={searchText}
      />
    );
  }
}

const GiphySearchContainerConnected = connect(
  state => ({
    searchText: selectors.getSearchText(state),
    gifs: selectors.getGifs(state),
    currentItemIndex: selectors.getCurrentItemIndex(state),
    error: selectors.getError(state),
    isFetching: selectors.getIsFetching(state),
  }),
  {
    onNext: actions.navigateNext,
    onPrev: actions.navigatePrev,
    setSearchText: actions.setSearchText,
    fetchGiphy: actions.fetchGiphy,
  }
)(GiphySearchContainer);
export { GiphySearchContainerConnected, GiphySearchContainer };
