import React from 'react';
import { shallow } from 'enzyme';
import { GiphySearchContainer } from './GiphySearchContainer';

const props = {
  currentItemIndex: 0,
  searchText: 'searchText',
  error: null,
  isFetching: false,
  gifs: [
    {
      title: 'title',
      url: 'url',
      shareUrl: 'shareUrl',
    },
  ],
  onSearch: jest.fn(),
  onNext: jest.fn(),
  onPrev: jest.fn(),
  setSearchText: jest.fn(),
  fetchGiphy: jest.fn(),
};

beforeEach(() => {
  props.setSearchText = jest.fn();
  props.fetchGiphy = jest.fn();
});

test('should render GiphySearch', () => {
  expect(shallow(<GiphySearchContainer {...props} />)).toMatchSnapshot();
});

describe('handleSearch method', () => {
  test('should call setSearchText regardless of searchText value', () => {
    const giphySearchContainer = new GiphySearchContainer(props);
    giphySearchContainer.handleSearch('');
    giphySearchContainer.handleSearch('searchText');

    expect(props.setSearchText).toHaveBeenCalledTimes(2);
  });

  test('should not call fetchGiphy', () => {
    const giphySearchContainer = new GiphySearchContainer(props);
    giphySearchContainer.handleSearch('');
    expect(props.fetchGiphy).not.toBeCalled();
  });

  test('should call fetchGiphy with searchText', () => {
    const giphySearchContainer = new GiphySearchContainer(props);
    const searchText = 'searchText';
    giphySearchContainer.handleSearch(searchText);
    expect(props.fetchGiphy).toBeCalledWith(searchText);
  });
});
